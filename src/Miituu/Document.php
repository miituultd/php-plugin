<?php

namespace Miituu;

class Document extends Model {

	protected $path = null;

	public $fields = array('id', 'company_id', 'answer_id', 'status', 'mime', 'filesize', 'views', 'url', 'expires', 'orig_filename', 'filename', 'created_at', 'updated_at', 'hostname', 'custom_data');

	public $mutable = array('status', 'mime', 'filesize', 'views', 'expires', 'orig_filename', 'filename', 'created_at', 'updated_at', 'hostname', 'custom_data');

    public $json = array('custom_data');

	public $has_status = true;

	public $relations = array(
		array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'answer',
            'model' => '\Miituu\Answer',
            'multiple' => false
        )
	);

	public function answer() {
		return Answer::where('id', $this->answer_id);
	}

  public function _create($data) {
    // Strip back the data to what's fillable, but also include type, or a default
    $post = array_intersect($data, $this->mutable);
    $post['type'] = array_key_exists('type', $data) ? $data['type'] : 'default';

    return $this->file( 'document', $data['document'] )->call('', $post, 'POST');
  }
}
