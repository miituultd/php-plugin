<?php

namespace Miituu;

class Playlist extends Model {

    protected $path = 'playlists';

    public $fields = array('id', 'company_id', 'user_id', 'name', 'created_at', 'updated_at', 'status', 'slug', 'custom_data', 'statistics');

    public $mutable = array('name', 'status', 'custom_data');

    public $json = array('custom_data');

    public $has_status   = true;

    public $relations = array(
        array(
            'key' => 'items',
            'model' => '\Miituu\Item',
            'multiple' => true
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
    );

    public function items() {
        return Item::where('playlist_id', $this->id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }

    /*
     *  Take an answer object or ID, and change it's position in the current collection
     */
    public function moveItem($item, $position) {
        // Get the ID from an answer object, or assume $answer is the ID
        $item_id = $item instanceof \Miituu\Item ? $item->id : $item;

        return $this->call('/order', array('item_id' => $item_id, 'to' => $position), 'POST');
    }
}
