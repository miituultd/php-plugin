<?php

namespace Miituu;

class Invoice extends Model {

	protected $path = 'billing/invoices';

	public $fields = array('uuid', 'state', 'invoice_number', 'total', 'created_at', 'download');

	public $mutable = array();

	public $relations = array(
    );
}