<?php

namespace Miituu;

class Notification extends Model {

	protected $path = 'notifications';

	public $fields = array('id', 'company_id', 'user_id', 'questionnaire_id', 'status', 'sound', 'message', 'created_at', 'updated_at', 'badge', 'message_id');

	public $mutable = array('status', 'message', 'created_at', 'updated_at', 'badge');

	public $has_status = true;

	public $relations = array(
		array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'questionnaire',
            'model' => '\Miituu\Questionnaire',
            'multiple' => false
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
	);

	public function user() {
		return User::where('id', $this->user_id);
	}

	public function questionnaire() {
		return Questionnaire::where('id', $this->questionnaire_id);
	}
}
