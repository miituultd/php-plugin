<?php

namespace Miituu;

class Mailinglist extends Model {

	protected $path = 'mailinglists';

	public $fields = array('id', 'company_id', 'user_id', 'name', 'description', 'created_at', 'updated_at', 'status');

	public $mutable = array('name', 'description', 'created_at', 'updated_at', 'status');

	public $has_status = true;

	public $relations = array(
		array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        ),
        array(
            'key' => 'addresses',
            'model' => '\Miituu\Address',
            'multiple' => true
        )
	);

	public function user() {
		return User::where('id', $this->user_id);
	}

	public function addresses() {
		return Address::where('mailinglist_id', $this->id);
	}
}
