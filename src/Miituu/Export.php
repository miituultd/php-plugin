<?php

namespace Miituu;

class Export extends Model {
    protected $path = 'exports';

    public $fields = array(
        'id',
        'company_id',
        'user_id',
        'questionnaire_id',
        'playlist_id',
        'name',
        'description',
        'locked',
        'pending_questions',
        'pending_answers',
        'pending_items',
        'include_images',
        'include_media',
        'only_published',
        'url',
        'progress',
        'status',
        'created_at',
        'updated_at',
        'video_format',
        'audio_format',
        'expires',
        'size'
    );

    public $mutable = array('name', 'description', 'status', 'created_at', 'updated_at');

    public $has_status   = true;

    public $status_titles   = array(
        self::STATUS_ACTIVE   => 'Ready',
        self::STATUS_DELETED  => 'Deleted',
        self::STATUS_QUEUED   => 'Queued',
        self::STATUS_PENDING  => 'Pending',
    );

    public $inclusions = array('questionnaire', 'playlist');

    public $relations = array(
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        ),
        array(
            'key' => 'questionnaire',
            'model' => '\Miituu\Questionnaire',
            'multiple' => false
        ),
        array(
            'key' => 'playlist',
            'model' => '\Miituu\Playlist',
            'multiple' => false
        )
    );

    public function user() {
        return User::where('id', $this->user_id);
    }

    public function questionnaire() {
        return Questionnaire::where('id', $this->questionnaire_id);
    }

    public function playlist() {
        return Playlist::where('id', $this->playlist_id);
    }
}
