<?php

namespace Miituu;

class Token extends Model {

    const UI_WEB = 1;
    const UI_APP = 2;

    protected $path = 'auth';

    public $fields = array('token', 'company_id', 'level_id', 'expires', 'updated_at', 'created_at', 'id', 'permissions', 'company');

    public $relations = array(
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        ),
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'respondent',
            'model' => '\Miituu\Respondent',
            'multiple' => false
        )
    );

    /*
     *  Take a company slug and request a public token
     */
    public static function publicAuth($slug, $include = false) {
        $include = $include ?: array('company', 'permissions');
        $token = Token::with($include)->call('/public', array('slug' => $slug));

        // If we we're successful, copy these details to the Api::static for easy access
        if ($token->success) $token->process();

        return $token;
    }

    /*
     *  Take an existing users's email address and password, and log them in
     */
    public static function login($email, $password, $interface = self::UI_WEB) {
        $token = Token::with(array('company', 'user', 'permissions'))
                      ->call('/login', array(
                        'email'           => $email,
                        'password'        => $password,
                        'user_interface'  => $interface,
                      ), 'POST', false);

        if ($token->success) $token->process();

        return $token;
    }

    /*
     *  Call the API to end the session of the current token
     */
    public function doLogout() {
        return $this->call('/logout');
    }


    /*
     *  When there is already a token, check with the API that it is still valid
     *  Also refreshes the company, permissions, and (if applicable) the user
     */
    public function check($include = false) {
        $include = is_array($include) ? $include : array();

        $this->with($include)->call();

        // If we we're successful, copy these details to the Api::static for easy access
        if ($this->success) $this->process();

        return $this;
    }

    /*
     *  After a token has been created or restored, this function is used to save it's details into the API model
     */
    private function process() {
        if ($this->company)        Api::$currentCompany       = $this->company;
        if ($this->user)           Api::$currentUser          = $this->user;
        if ($this->user && $this->user->permissions)    Api::$permissions          = $this->user->permissions;

        if ($this->company && self::el($this->company, 'boltons'))
                                   Api::$boltons       = self::el($this->company, 'boltons');

        if ($this->user && self::el($this->user, 'permissions'))
                                   Api::$permissions   = self::el($this->user, 'permissions');
    }

}
