<?php

namespace Miituu;

class Partnercode extends Model {
	protected $path = 'partnercodes';

	public $fields = array( 'id', 'code', 'reseller_id', 'status', 'discount', 'created_at', 'updated_at' );

	public $mutable = array('code', 'discount', 'reseller_id', 'status');

	public $json = array();

    public $has_status  = false;

    public $relations = array(
        array(
            'key' => 'reseller',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'resells',
            'model' => '\Miituu\Resell',
            'multiple' => true
        )
    );
}