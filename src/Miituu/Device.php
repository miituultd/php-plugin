<?php

namespace Miituu;

class Device extends Model {

	protected $path = 'devices';

	public $fields = array('id', 'application_id', 'user_id', 'token', 'status', 'device_type', 'device_arn', 'created_at', 'updated_at');

	public $mutable = array('status', 'device_type', 'created_at', 'updated_at');

    public $has_status = true;

    public $relations = array(
    	array(
            'key' => 'application',
            'model' => '\Miituu\Application',
            'multiple' => false
        ),
        array(
            'key' => 'companies',
            'model' => '\Miituu\Company',
            'multiple' => true
        )
    );

    public function application() {
    	return Application::where('id', $this->application_id);
    }
}
