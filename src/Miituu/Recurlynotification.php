<?php

namespace Miituu;

class Recurlynotification extends Model {

	protected $path = 'miituu/recurly_notifications';

	public $fields = array( 'id', 'company_id', 'type', 'data', 'created_at', 'updated_at' );

	public $mutable = array();

    public $json = array();

    public $has_status  = false;

    public $relations = array(
    	array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        )
    );
}