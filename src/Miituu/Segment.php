<?php

namespace Miituu;

class Segment extends Model {

	protected $path = 'segments';

	public $fields = array('id', 'status', 'name', 'description', 'search', 'created_at', 'updated_at', 'company_id');

	public $mutable = array('status', 'name', 'description', 'search', 'created_at', 'updated_at');

	public $has_status = true;

	public $relations = array(
		array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'campaigns',
            'model' => '\Miituu\Campaign',
            'multiple' => true
        )
	);
}