<?php

namespace Miituu;

class ShortUrl extends Model {

    protected $path = 'shorturls';

    public $fields = array('id', 'company_id', 'slug', 'url', 'qr_image', 'created_at', 'updated_at');

    public $mutable = array();

    public $relations = array(
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        )
    );

    // Shorten a URL
    public function _shorten($url) {
        // Make the call
        return $this->call('/shorten', array(
            'url' => $url
        ));
    }

    // Expand a slug
    public function _expand($slug) {
        // Make the call
        return $this->call('/expand', array(
            'slug' => $slug
        ));
    }

    // Return a complete short URL
    public function getShort_url() {
        return Api::$short_base . $this->slug;
    }

}
