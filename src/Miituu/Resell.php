<?php

namespace Miituu;

class Resell extends Model {
	protected $path = 'resells';

	public $fields = array( 'id', 'company_id', 'reseller_id', 'partnercode_id', 'commission', 'discount', 'commission_actual', 'discount_actual', 'reseller_percent', 'reseller_actual', 'created_at', 'updated_at' );

	public $mutable = array( 'commission', 'discount' );

    public $json = array();

    public $has_status  = false;

    public $relations = array(
    	array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'partnercode',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'reseller',
            'model' => '\Miituu\Company',
            'multiple' => false
        )
    );
}