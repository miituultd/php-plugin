<?php

namespace Miituu;

class Company extends Model {

    protected $path = 'companies';

    public $fields = array(
        // These are the standard company fields
        'id', 'name', 'status', 'created_at', 'updated_at', 'slug', 'package_id', 'bandwidth_used', 'storage_used', 'over_usage', 'color_scheme', 'use_title', 'users_used', 'billing_status', 'emails_used', 'use_protected', 'type', 'custom_data',
        // This lets us store boltons
        'boltons',
        // And these are really 'meta' fields, should maybe be a different model, but this is hopefully more convenient
        'address', 'city', 'region', 'postcode', 'country', 'telephone', 'email', 'vat',
        // And these are for data usage
        'media_size', 'image_size', 'export_size', 'document_size', 'media_duration', 'max_size', 'free_size'
    );

    public $mutable = array(
        // Company mutable
        'name', 'color_scheme', 'color', 'use_title', 'custom_data',
        // Meta mutable
        'address', 'city', 'region', 'postcode', 'country', 'telephone', 'email', 'vat'
    );

    public $inclusions = array('images', 'logos', 'users', 'package', 'boltons', 'meta', 'user', 'level');

    /*
     *  Statuses used for companies
     */
    const STATUS_INACTIVE     = 10; // No API interaction is allowed, at all
    const STATUS_ACTIVE       = 30; // All API interaction, happy days
    const STATUS_BILLING      = 20; // Only API interaction required for billing is allowed
    const STATUS_DELETED      = 90; // It basically doesn't exist

    public $status_titles     = array(
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_ACTIVE   => 'Active',
        self::STATUS_BILLING  => 'Billing',
        self::STATUS_DELETED  => 'Deleted'
    );

    /*
     *  List of all possible billing statuses
     */
    const BILLING_PENDING     = 0;  // Not even registered with Recurly yet
    const BILLING_REGISTERED  = 10; // Registered with Recurly, but not fully setup
    const BILLING_CURRENT     = 20; // Active, up-to-date subscription - we like this
    const BILLING_OVERDUE     = 30; // Payment failed or something - we don't like this
    const BILLING_CANCELED    = 40; // User canceled - we hate this

    public $billing_status_titles = array(
        self::BILLING_PENDING     => 'Pending',
        self::BILLING_REGISTERED  => 'Registered',
        self::BILLING_CURRENT     => 'Current',
        self::BILLING_OVERDUE     => 'Overdue',
        self::BILLING_CANCELED    => 'Canceled'
    );

    /*
     *  List of all company types, these are used internally
     */
    const TYPE_LIVE           = 10;
    const TYPE_INTERNAL       = 20;
    const TYPE_PARTNER        = 30;
    const TYPE_OLD            = 40;

    public $type_titles = array(
        self::TYPE_LIVE       => 'Live',
        self::TYPE_INTERNAL   => 'Internal',
        self::TYPE_PARTNER    => 'Partner',
        self::TYPE_OLD        => 'Old'
    );

    public $relations = array(
        array(
            'key' => 'actions',
            'model' => '\Miituu\Action',
            'multiple' => true
        ),
        array(
            'key' => 'answers',
            'model' => '\Miituu\Answer',
            'multiple' => true
        ),
        array(
            'key' => 'documents',
            'model' => '\Miituu\Document',
            'multiple' => true
        ),
        array(
            'key' => 'groups',
            'model' => '\Miituu\Group',
            'multiple' => true
        ),
        array(
            'key' => 'images',
            'model' => '\Miituu\Image',
            'multiple' => true
        ),
        array(
            'key' => 'items',
            'model' => '\Miituu\Item',
            'multiple' => true
        ),
        array(
            'key' => 'logos',
            'model' => '\Miituu\Image',
            'multiple' => true
        ),
        array(
            'key' => 'media',
            'model' => '\Miituu\Media',
            'multiple' => true
        ),
        array(
            'key' => 'package',
            'model' => '\Miituu\Package',
            'multiple' => false
        ),
        array(
            'key' => 'playlists',
            'model' => '\Miituu\Playlist',
            'multiple' => true
        ),
        array(
            'key' => 'questionnaires',
            'model' => '\Miituu\Questionnaire',
            'multiple' => true
        ),
        array(
            'key' => 'tokens',
            'model' => '\Miituu\Token',
            'multiple' => true
        ),
        array(
            'key' => 'users',
            'model' => '\Miituu\User',
            'multiple' => true
        ),
        array(
            'key' => 'walls',
            'model' => '\Miituu\Wall',
            'multiple' => true
        )
    );

    public function answers() {
        return new Answer;
    }

    public function groups() {
        return new Group;
    }

    public function images() {
        return new Image;
    }

    public function items() {
        return new Item;
    }

    public function logos() {
        return Image::where('type', 'company_logo');
    }

    public function media() {
        return new Media;
    }

    public function playlists() {
        return new Playlist;
    }

    public function questionnaires() {
        return new Questionnaire;
    }

    public function users() {
        return new User;
    }

    public function walls() {
        return new Wall;
    }

    public function package() {
        return Package::overridePath('billing/package');
    }

    /*
     *  Get the company info, but include meta data
     */
    public function meta() {
        return $this->get('/meta');
    }

    public function storage() {
        return $this->get('/storage');
    }

    /*
     *  Save the company, but if we have meta data, save to the meta endpoint
     */
    public function _save($data = array()) {
        // Save to meta endpoint
        if ($this->address) {
            $data = array_merge($this->toArray(), $data);
            return $this->call('/meta', $data, 'POST');

        // Use the regular save method
        } else {
            return parent::save($data);
        }
    }

    public function getBilling_status_title() {
        if (array_key_exists($this->billing_status, $this->billing_status_titles)) {
            return $this->billing_status_titles[$this->billing_status];
        } else {
            return 'Error';
        }
    }

    public function getType_title() {
        if (array_key_exists($this->type, $this->type_titles)) {
            return $this->type_titles[$this->type];
        } else {
            return 'Error';
        }
    }

    public function bandwidth($days = 30) {
        return $this->call('/bandwidth', array(
            'days' => $days
        ), 'GET', true, true);
    }

    public function _register( $data = array() ){
        
        return $this->call('/register', $data, 'POST', false);
    }

}
