<?php

namespace Miituu;

class Group extends Model {

    protected $path = 'groups';

    public $fields = array('id', 'company_id', 'user_id', 'name', 'created_at', 'updated_at', 'status');

    public $mutable = array('name', 'status');

    public $inclusions = array('questionnaires', 'questions', 'user', 'statistics');

    public $relations = array(
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'questionnaires',
            'model' => '\Miituu\Questionnaire',
            'multiple' => true
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
    );

    public function questionnaires() {
        return Questionnaire::where('group_id', $this->id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }

}
