<?php

namespace Miituu;

use \Miituu\Company as Company;
use \Miituu\Collection as Collection;
use \Miituu\Answer as Answer;
use \Miituu\Media as Media;
use \Miituu\Rendition as Rendition;

class Model extends Api implements \Iterator {

    // Access this to indicate if the call was successful or not (BOOL)
    public $success             = null;
    // The HTTP code returned by the API
    public $request_status      = null;

    // Params to be used for requests
    private $params             = array();
    // Params to be used for including relations in the requests
    private $withs              = array();
    // The current position for iterating over multiple items
    private $position           = 0;
    // An array to save multiple items in
    private $results            = array();
    // A variable to store the 'raw' json response from the API
    private $response_body      = null;

    // Store a URL we need to send the user to
    public $redirect            = null;

    // These are configured in each model
    public $fields              = array();
    public $mutable             = array();
    // When these fields are get/set they'll be automatically JSON encoded/decoded
    public $json                = array();
    // This will be populated with are errors returned by the API
    public $errors              = array();
    // Indicates whether the model/status endpoint is applicable
    public $has_status          = false;
    // Indicates whether the model/promise_media endpoint is applicable
    public $has_promise_media   = false;

    // All unmodified data for the current item
    private $clean              = array();
    // All modified data for the current item
    private $dirty              = array();
    // Temporarily store files to transfer in the next request
    private $files              = array();
    // All related models which are present
    private $related            = array();
    // Details of the types of relations the model can have
    protected $relations        = array();

    // Paging information
    private $current_page       = null;
    private $total_items        = null;
    private $total_pages        = null;
    private $per_page           = null;
    private $next_page          = null;
    private $prev_page          = null;

    // These can be overridden for specific models
    public $status_titles   = array(
        self::STATUS_PRIVATE    => 'Private',
        self::STATUS_PROTECTED  => 'Protected',
        self::STATUS_PUBLIC     => 'Public',
        self::STATUS_FAILED     => 'Failed',
        self::STATUS_PROMISED   => 'Promised',
        self::STATUS_PENDING    => 'Pending',
        self::STATUS_DELETED    => 'Deleted',
        self::STATUS_OPEN       => 'Open',
        self::STATUS_CLOSED     => 'Closed',
    );

    // These statuses may be selected, this can be overridden per-model
    public static $standard_statuses = array(
        self::STATUS_PRIVATE,
        self::STATUS_PROTECTED,
        self::STATUS_PUBLIC
    );

    /*
     *  Make a call to the API, saves the result into the current model and return $this
     */
    public function _call($endpoint = '', $data = array(), $method = 'GET', $auth = true, $return_as = false)
    {
        // So we can record the entire duration
        $start = time();

        // Initialize Guzzle
        $client = new \Guzzle\Http\Client(self::$base, array(
            'request.options' => array(
                'proxy'   => parent::$proxy
            )
        ));

        // If auth is available and not disabled, create headers with the token
        $headers = ($auth && self::$token) ? array('X-App-Token' => self::$token->token) : array();

        // Add any paging settings to the params
        if ($this->per_page)     $this->params['per_page'] = $this->per_page;
        if ($this->current_page) $this->params['page']     = $this->current_page;

        // Add relations to the params
        if (count($this->withs)) $this->params['include'] = implode(',', $this->withs);

        // If we have an array of params, add that to the data
        if ($this->params) $data = array_merge($this->params, $data);

        // Append the endpoint to the current model's path
        $url = $this->path . $endpoint;

        // Configure GET request
        if ($method == 'GET') {
            $response = $client->get($url, $headers, array(
                'query' => $data,
                'exceptions' => false
            ))->send();

        } else if ($method == 'DELETE') {
            $response = $client->delete($url, $headers, $data, array(
                'exceptions' => false
            ))->send();

        // Or POST request
        } else if ($method == 'POST') {

            // Make the request in advance so we can add any files
            $request = $client->post($url, $headers, $data, array(
                'exceptions' => false
            ));

            // Check for files to send
            foreach ($this->files as $key => $path) {
                $finfo = @new \finfo(FILEINFO_MIME);
                $mime = @$finfo->file($path);
                // Add the file
                $request->addPostFile($key, $path, $mime);
            }
            // Clear the files array so we don't include them in the next request
            $this->files = array();

            // Now send the request
            $response = $request->send();
        }

        // Parse the response body
        $body = $response->json();
        // Save the response body
        $this->response_body = $body;

        // Save the status code
        $this->request_status = $response->getStatusCode();

        // If failure, save the error messages
        if ($response->getStatusCode() != 200) {
            $this->success  = false;
            $this->errors   = self::el($body, 'messages');
            $_redirect = self::el($body, 'url');

            if( $_redirect ){
                $parsed = parse_url($_redirect);
                $parsed['query'] = urlencode($parsed['query']);

                $_redirect = $parsed['scheme'].'://'.$parsed['host'].$parsed['path'].'?'.$parsed['query'];
            }

            $this->redirect = $_redirect;

        // If success, save all responses
        } else {

            // Should we return the results into a different model?
            if ($return_as instanceof Model) {
                $return_as->success = true;

                // We've received multiple items, append them, save the paging info, and select the current item
                if (self::el($body, 'body', false)) {
                    $return_as->setItems(self::el($body, 'body'));
                    $return_as->setPaging($body);
                    $return_as->current();

                // If not, just fill the object with the current info
                } else {
                    $return_as->fill($body, true);
                }

            // Return the raw data to a callback
            } else if ($return_as instanceof Closure || $return_as === true) {
                // Don't return as default

            // No, use this
            } else {

                $this->success = true;

                // We've received multiple items, append them, save the paging info, and select the current item
                if (self::el($body, 'body', false)) {
                    $this->results = array_merge($this->results, self::el($body, 'body'));
                    $this->setPaging($body);
                    $this->current();

                // If not, just fill the object with the current info
                } else {
                    $this->fill($body, true);
                }
            }

        }

        self::$calls[] = array(
            'url'           => $url,
            'status'        => $response->getStatusCode(),
            'data'          => $data,
            'duration'      => time() - $start,
            'size'          => $response->getContentLength()
        );

        // Call the closure and return the result
        if ($return_as instanceof Closure) {
            return $return_as($body);

        // Just return the raw body
        } else if ($return_as === true) {
            return $body;

        // Or return $this / the instance
        } else {
            return $return_as ? $return_as : $this;
        }
    }

    /*
     *  Add a file path to upload
     */
    public function _file($key, $path) {
        if (!file_exists($path)) throw new \Exception('File does not exist at path: '.$path);

        $this->files[$key] = $path;

        return $this;
    }

    /*
     *  Request a particular item by ID
     */
    public function _find($id, $params = array()) {
        return $this->call('', array('id' => $id) + $params);
    }

    /*
     *  Work with a particular ID, but do not query the API for it
     */
    public function _id($id) {
        $this->id = $id;
        return $this;
    }

		/*
		 *  Request a collection of items by their ids
		 */
		public function _findMany($ids, $params = array()) {
			return $this->call('', array('ids' => $ids) + $params);
		}

    /*
     *  Usually the get function is a simple call to the model's path
     *  This can be overridden by models if required
     */
    public function _get($path = '', $params = array()) {
        return $this->call($path, $params);
    }

    /*
     *  Call the model's path as above but return the raw response from the API
     */
    public function _getRaw($path = '') {
        /* parameters are:
            path - appended to the model's endpoint
            data - query string to append, in this case we just hit an endpoint without parameters
            we use a GET request
            auth - true, we need to authenticate with a token

            last parameter return as - if set to true it will return the raw response from the API
        */
        return $this->call($path, array(), 'GET', true, true);
    }

    public function getRawResponse(){
        return $this->response_body;
    }

    /*
     *  Change the status of the object using the /model/status endpoint
     *  This triggers any cascading changes in related models
     *  Optionally specify an ID to use this method statically
     */
    public function _setStatus($status, $_id = null) {
        if (!$this->has_status) throw new \Exception(get_class($this).' does not use the /model/status endpoint');

        if ($this->exists() && !$_id) {
            $id = $this->id;

        } else if (!$this->exists() && $_id) {
            $id = $_id;

        } else {
            throw new \Exception('Invalid conditions for set_status()');
        }

        return $this->call('/status', array('id' => $id, 'status' => $status), 'POST');
    }

    /*
     *  Access the set_status method and set the item to public
     */
    public function _publish() {
        return $this->set_status(self::STATUS_PUBLIC);
    }

    /*
     *  Only applicable to certain models, creates a promised media object
     */
    public function promiseMedia($type = null) {
        if ($type) $this->type = $type;

        $media = new Media;
        return $this->call('/promise_media', array('type' => $this->type, 'id' => $this->id), 'GET', true, $media);
    }

    /*
     *  Take a string or array of related models to include
     */
    public function _with() {
        // Build an array of everything to include
        $withs = array();

        // Loop through all the arguments
        $_withs = func_get_args();
        foreach ($_withs as $w) {
            // Add everything to the $withs, be it array or string
            if (is_array($w))  $withs   = array_merge($withs, $w);
            if (is_string($w)) $withs[] = $w;
        }

        $this->withs = $withs;

        return $this;
    }

    /*
     *  Specify params for the call, such as limiting answers by collection_id
     */
    public function _where($fields, $value = null) {
        if (!is_array($fields)) $fields = array($fields => $value);

        $this->params = array_merge($this->params, $fields);

        return $this;
    }

    /*
     *  Specify how the results should be ordered
     */
    public function _orderBy($field, $direction = null) {
        $this->where('sort', $field);

        if ($direction) $this->where('order', $direction);

        return $this;
    }

    /*
     *  Take a single value or array to restrict results to
     */
    public function _status($statuses) {
        $this->_where('status', $statuses);

        return $this;
    }

    /*
     *  Send all information for the model to the API to update it
     */
    public function _save($data = array()) {
        $data = array_merge($this->toArray(), $data);
        return $this->call('', $data, 'POST');
    }

    /*
     *  Save the current paging information from the API
     */
    public function setPaging($body) {
        $this->current_page  = self::el($body, 'current_page');
        $this->total_items   = self::el($body, 'total_items');
        $this->total_pages   = self::el($body, 'total_pages');
        $this->per_page      = self::el($body, 'per_page');

        $this->next_page     = $this->current_page < $this->total_pages ? $this->current_page + 1 : null;
        $this->prev_page     = $this->current_page > 1 ? $this->current_page - 1 : null;
    }

    /*
     *  Increment the current_page, make the request, and return the result
     */
    public function nextPage() {
        if ($this->current_page >= $this->total_pages) return false;

        $this->current_page++;

        return $this->call();
    }

    /*
     *  Specify which page to request from the API
     */
    public function _page( $int ) {
        $this->current_page = $int;

        return $this;
    }

    /*
     *  Set the per_page parameter, specify how many items the API should return
     */
    public function _perPage( $int ) {
        $this->per_page = $int;

        return $this;
    }

    /*
     *  Wraps the _perPage function
     */
    public function _take( $int ) {
        return $this->_perPage($int);
    }

    /*
     *  Take an array or object and populate the models fields with it
     */
    public function _fill($data, $reset = false) {
        if ($reset) $this->reset();

        // Look for each field and fill with data
        foreach ($this->fields as $field) {
            // If we're resetting this model we can fill all valid fields
            if ($reset && in_array($field, $this->fields)) {
                $this->clean[$field] = in_array($field, $this->json) ? self::jsonEncode(self::el($data, $field)) : self::el($data, $field);

            // If not, only fill the valid ones, into the dirty array
            } else if (in_array($field, $this->mutable) || !$this->exists) {
                $this->dirty[$field] = in_array($field, $this->json) ? self::jsonEncode(self::el($data, $field)) : self::el($data, $field);
            }
        }

        // Look for any relations we should create
        foreach ($this->relations as $details) {
            // Null is a valid value and will stop us rechecking for relations which don't exist
            $model_data = self::el($data, $details['key'], 'NOTPRESENT');
            if ($model_data != 'NOTPRESENT') {

                // To make sure if ($model) is an accurate indication of existence, store false if nothing was returned
                if ($model_data == null) {
                    $this->related[$details['key']] = false;

                } else if ($details['multiple']) {
                    $this->related[$details['key']] = $details['model']::setItems($model_data);

                } else {
                    $this->related[$details['key']] = $details['model']::fill($model_data, true);
                }
            }
        }

        return $this;
    }

    /*
     *  Fill a model with multiple items, used when creating included related models
     */
    public function _setItems($data) {
        $this->results = $data;

        return $this;
    }

    /*
     *  If this is a list of multiple items, return the first one
     */
    public function _first() {
        // If we don't have any results, try to call one from the API
        if (!count($this->results)) $this->take(1)->get();

        // If we still didn't get one, fail
        if (!count($this->results)) return $this;

        // Return the first result
        $this->fill( $this->results[0], true );
        return $this;
    }

    /*
     *  Reset all parameters to empty
     */
    public function reset() {
        $this->clean   = array();
        $this->dirty   = array();
        $this->related = array();
    }

    /*
     *  Returns BOOL, true if the object is new or has been modified
     */
    public function _dirty() {
        return !$this->exists || count($this->dirty);
    }

    /*
     *  Return BOOL, true if the object exists in the API
     */
    public function exists() {
        return $this->id ? true : false;
    }

    /*
     *  Return an associative array of modified parameters
     */
    public function getDirty() {
        return $this->dirty;
    }

    /*
     *  Return true if there are multiple items to loop through
     */
    public function multiple() {
        return count($this->results) > 0;
    }

    /*
     *  Return all fields in an associative array, including dirty items
     *  Optionally, if there are multiple items, return them all as an array
     *  Optionally, return an object instead, probably use the to_object method though
     */
    public function toArray($as_object = false, $all = null) {
        // Always returning an array for empty objects makes things easier
        // if (!$this->multiple() && !$this->exists()) return array();

        // Unless all was specific, work it out for ourselves
        if ($all === null) $all = $this->multiple();

        // Return all items?
        if ($all) {
            $result = array();
            foreach ($this as $item) {
                // We're getting recursive here, make sure we don't loop over ourself infinitely!
                $result[] = $item->toArray($as_object, false);
            }
            return $result;
        }

        // Get all the relations for this object
        $relations = array();
        foreach ($this->relations as $rel) {
            if (isset($this->{$rel['key']})) {
                // Convert the related model to an array too,
                $relations[ $rel['key'] ] = $this->{$rel['key']}->toArray();
            }
        }

        // Put it all together
        $data = array_merge($this->clean, $this->dirty, $relations);

        // Clean NULL values
        $return = array();
        foreach ($data as $key => $value) {
            if ($value != null && $value != '') $return[$key] = $value;
        }

        // Return it as an abject?
        if ($as_object) {
            return (object)$data;
        } else {
            return $data;
        }
    }


    /*
     *  Wraps the toArray function, but instructs it to return objects
     *  Getting an object from a function called toArray would be pretty confusing!
     */
    public function toObject($all = null) {
        return $this->toArray(true, $all);
    }

    /*
     *  Iterator methods, these allow us to loop through results
     */
    function rewind() {
        $this->position = 0;
    }

    function current() {
        return $this->fill( $this->results[$this->position], true );
    }

    function key() {
        return $this->position;
    }

    function next() {
        ++$this->position;
    }

    function valid() {
        return isset($this->results[$this->position]);
    }

    function count() {
        return count($this->results);
    }

    /*
     *  Getter for status_name
     */
    public function getStatus_title() {
        return array_key_exists($this->status, $this->status_titles) ? $this->status_titles[$this->status] : $this->status;
    }

    /*
     *  Format a size field from bytes to whatever is appropriate
     */
    public function getSize_formatted() {
        $size = $this->size;
        if (!$size) return 'N/A';

        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB');
        $u = 0;
        while($size >= 1024) {
            $size /= 1024;
            $u++;
        }
        return number_format($size, 1).' '.$units[$u];
    }

    /*
     *  Return an array of validation rules for a given field name
     */
    public function validationRules($field_name) {
        if ( !property_exists($this, 'validation') || !array_key_exists($field_name, $this->validation) ) {
            return array();
        }

        return explode( '|', $this->validation[$field_name] );
    }

    /*
     *  Handle dynamic retrieval of attributes
     */
    public function __get($key) {
        // Simply return the property, if one exists
        if (isset($this->$key)) return $this->$key;

        // Return a relation if one exists
        if (isset($this->related[$key])) return $this->related[$key];

        // Check for model getter method
        if (method_exists($this, 'get'.ucfirst($key))) {
            return $this->{'get'.ucfirst($key)}();
        }

        // If a method exists for it, it'll be retrieving a relationship, so get it
        if (method_exists($this, $key)) {
            $tmp = $this->$key();
            if ($tmp instanceof Model) {
                // Save this for later
                $this->related[$key] = $tmp->get();
                return $this->related[$key];
            }
            return $tmp;
        }

        // If there is a modified param for this key, return it
        if (self::el($this->dirty, $key) !== null)
            // JSON decide the field if it's in the json list
            return in_array($key, $this->json) ? self::jsonDecode($this->dirty[$key]) : $this->dirty[$key];

        // If there is a clean param for this key, return it
        if (self::el($this->clean, $key) !== null)
            // JSON decide the field if it's in the json list
            return in_array($key, $this->json) ? self::jsonDecode($this->clean[$key]) : $this->clean[$key];

        // We got nothing, for JSON fields return an array, or null
        return in_array($key, $this->json) ? array() : null;
    }

    /*
     *  Return a value/title array of relevant status option
     */
    public static function statusOptions() {
        $instance = new static;

        $return = array();
        foreach (self::$standard_statuses as $key) {
            if ($key == self::STATUS_PROTECTED && !self::$currentCompany->use_protected) continue;
            $return[$key] = $instance->status_titles[$key];
        }

        return $return;
    }

    /*
     *  Convert a status value into a title
     */
    public static function statusTitle($key) {
        $instance = new static;
        return (array_key_exists($key, $instance->status_titles)) ? $instance->status_titles[$key] : null;
    }

    /*
     *  Return the type of object, basically get_class but removes 'Miituu\'
     */
    public function getClass() {
        return str_replace('Miituu\\', '', get_class($this));
    }

    /*
     *  Check for related it ID and return true/false
     */
    public function hasRelation($_key, $id = null) {
        // Handle the first param being an instance
        if ($_key instanceof Model) {
            $key = $this->findKeyFromInstance($_key);
            $id = $_key->id;
        } else {
            $key = $_key;
        }

        // If a key wasn't find, that's a problem
        if (!$key) throw new \Exception('Relation key '.$key.' not found on '.$this->class);

        // If this item doesn't find any relations for the key, it isn't related
        if (!$this->$key) return false;

        foreach ($this->$key as $item) {
            if ($item->id == $id) return true;
        }

        return false;
    }

    /*
     *  Take an instance or class string and try to find a key from it
     */
    public function findKeyFromInstance($item) {
        // Model instance
        if ($item instanceof Model) {
            $class = strtolower(get_class($item));
        // String
        } else {
            $class = strtolower($item);
        }

        // Remove the namespace
        $key = str_replace('miituu\\', '', $class);

        // Look for singular
        foreach ($this->relations as $relation) {
            if ($relation['key'] == $key) return $key;
        }

        // Look for plural
        // TODO: This probably isn't going to always work
        $key .= 's';
        foreach ($this->relations as $relation) {
            if ($relation['key'] == $key) return $key;
        }

        return false;
    }

    /*
     *  Handle dynamic setting of parameters, only items in the mutable array can be updated
     */
    public function __set($key, $value) {
        if ($this->exists == false || in_array($key, $this->mutable)) {
            // JSON encode the field if it's in the json list
            if (in_array($key, $this->json)) {
                $this->dirty[$key] = self::jsonEncode($value);
            } else {
                $this->dirty[$key] = $value;
            }
            return $this->dirty[$key];
        } else {
            foreach ($this->relations as $rel) {
                if ($rel['key'] == $key) return $this->related[$key] = $value;
            }
            return false;
        }
    }

    /*
     *  Take dynamic method calls and map them to the correct function
     */
    public function __call($method, $parameters) {
        // If the requested method is available with an underscore prefix, call it
        if (method_exists($this, '_'.$method))
            return call_user_func_array(array($this, '_'.$method), $parameters);
    }

    /*
     *  Take static calls and create an instance of the model
     */
    public static function __callStatic($method, $parameters)
    {
        // Create a new instance of the called class
        $model = get_called_class();

        // Call the requested method on the newly created object
        return call_user_func_array(array(new $model, $method), $parameters);
    }

    /*
     *  This isn't the function your looking for
     */
    public function _overridePath($path) {
        $this->path = $path;

        return $this;
    }
}
