<?php

namespace Miituu;

class Bolton extends Model {

    protected $path = 'billing';

    public $fields = array('id', 'name', 'code', 'maximum_quantity', 'created_at', 'updated_at', 'quantity', 'used', 'recurring_fee', 'setup_fee');

    public $mutable = array('name', 'created_at', 'updated_at');

    public $relations = array(
        array(
            'key' => 'companies',
            'model' => '\Miituu\Company',
            'multiple' => true
        )
    );

    /*
     *  Override the modal get function, boltons have a different endpoint to usual
     */
    public function _get($path = '') {
        return $this->get('/all_boltons');
    }

    // Remove a bolton from the company
    public function _remove($id, $params = array()) {
        // Put the ID into an array and merge it with any extras
        $data = array_merge($params, array('id' => $id));

        // Make the call
        return $this->call('/bolton', $data, 'DELETE');
    }

    // Add or update a bolton to the company
    public function _add($id, $quantity = 1, $params = array()) {
        // Put the ID into an array and merge it with any extras
        $data = array_merge($params, array(
            'id' => $id,
            'quantity' => $quantity
        ));

        // Make the call
        return $this->call('/bolton', $data, 'POST');
    }
}
