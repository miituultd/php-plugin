<?php

namespace Miituu;

class Respondent extends Model {

    protected $path = 'respondents';

    public $fields = array('id', 'questionnaire_id', 'ip_address', 'created_at', 'updated_at', 'status', 'progress', 'accepted', 'search', 'custom_data');

    public $mutable = array('custom_data');

    public $json = array('custom_data');

    public $relations = array(
        array(
            'key'       => 'answers',
            'model'     => '\Miituu\Answer',
            'multiple'  => true
        ),
        array(
            'key'       => 'questionnaire',
            'model'     => '\Miituu\Questionnaire',
            'multiple'  => false
        ),
        array(
            'key'       => 'token',
            'model'     => '\Miituu\Token',
            'multiple'  => false
        ),
        array(
            'key'       => 'userfieldanswers',
            'model'     => '\Miituu\Userfieldanswer',
            'multiple'  => true
        )
    );

    public function answers() {
        return Answer::where('respondent_id', $this->id);
    }

    public function userfieldanswers() {
        throw new \Exception('Access userfieldanswers by including them when accessing respondents.');
    }

    /*
     *  Getter method, will try to return the respondent's name
     */
    public function getName() {
        // If we couldn't get any userfield answers there's not much we can do
        $userfieldanswers = $this->userfieldanswers;
        if (!$userfieldanswers) return null;

        // Look for a userfieldanswer with for a userfield of type name
        foreach ($userfieldanswers as $userfieldanswer) {
            if ($userfieldanswer->userfield && $userfieldanswer->userfield->type == Userfield::TYPE_NAME) {
                return $userfieldanswer->answer;
            }
        }

        // Didn't get one? Just return the first userfieldanswer
        if ($first = $this->userfieldanswers->first()) {
            return $first->answer;
        }

        // We failed
        return null;
    }

    /*
     *  Create a new respondent with the provided questionnaire ID and userfields
     */
    public function _create( $questionnaire_id, $userfields, $story_answer = false) {
        return $this->call('', array(
            'questionnaire_id' => $questionnaire_id,
            'userfields'       => $userfields,
            'story_answer'     => $story_answer,
        ), 'POST');
    }

    /*
     *  Indicate whether agreement has been specified for the disclaimer
     */
    public function _disclaimer($accepted, $_id = null) {
        $id = $this->id ?: $_id;
        return $this->call('/disclaimer', array(
            'id' => $id,
            'accepted' => $accepted
        ), 'POST');
    }
}
