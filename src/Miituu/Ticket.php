<?php

namespace Miituu;

class Ticket extends Model {

    protected $path = 'tickets';

    public $fields = array('id', 'number', 'company_id', 'user_id', 'created_at', 'updated_at', 'closed_at', 'status', 'first_answer_id');

    public $mutable = array('first_answer_id');

    public $json = array();

    public $has_status = true;

    public $status_titles   = array(
        self::STATUS_OPEN   => 'Open',
        self::STATUS_CLOSED => 'Closed'
    );

    public $relations = array(
        array(
            'key'       => 'answers',
            'model'     => '\Miituu\Answer',
            'multiple'  => true
        ),
        array(
            'key'       => 'user',
            'model'     => '\Miituu\User',
            'multiple'  => false
        )
    );

    public function answers() {
        return Answer::where('ticket_id', $this->id)->where('status != '.Answer::STATUS_DELETED);
    }

    public function _add( $params ) {
        return $this->call('/add', $params, 'POST');
    }

    /*
     *  request a particular ticket by number
     */
    public function _findByNumber($number, $params = array()) {
        return $this->call('', array('number' => $number) + $params);
    }

    /*
     *  request a particular ticket by answer id
     */
    public function _findByAnswerId($answerId, $params = array()) {
        return $this->call('', array('answer_id' => $answerId) + $params);
    }

    /**
     * request own tickets.
     */
    public function _findOwn($params = array()) {
        return $this->call('/own', $params);
    }


    /**
     * request all tickets.
     */
    public function _findAll($params = array()) {
        return $this->call('', $params);
    }

}
