<?php

namespace Miituu;

class Question extends Model {

    protected $path      = 'questions';

    public static $types = array('text', 'multichoice', 'video', 'audio');

    public $fields       = array('id', 'questionnaire_id', 'media_id', 'name', 'accepts', 'text', 'multichoice', 'required', 'status', 'answers_status', 'order', 'created_at', 'updated_at', 'type', 'maximum_selections', 'maximum_duration', 'user_id', 'search', 'statistics', 'custom_data');

    public $mutable      = array('name', 'accepts', 'text', 'multichoice', 'required', 'order', 'type', 'maximum_selections', 'maximum_duration', 'status', 'answers_status', 'custom_data');

    public $json         = array('multichoice', 'accepts', 'custom_data');

    public $has_status   = true;

    public $has_promise_media   = true;

    public $validation = array(
        'name'               => 'required|between:2,175',
        'accepts'            => 'required|accepts',
        'multichoice'        => 'multichoice',
        'questionnaire_id'   => 'required|integer|questionnaire',
        'media_id'           => 'integer',
        'order'              => 'integer|between:0,99',
        'required'           => 'integer|between:0,1',
        'type'               => 'required|type',
        'maximum_selections' => 'integer',
        'answers_status'     => 'integer'
    );

    public $relations = array(
        array(
            'key' => 'answers',
            'model' => '\Miituu\Answer',
            'multiple' => true
        ),
        array(
            'key' => 'media',
            'model' => '\Miituu\Media',
            'multiple' => false
        ),
        array(
            'key' => 'questionnaire',
            'model' => '\Miituu\Questionnaire',
            'multiple' => false
        ),
        array(
            'key' => 'images',
            'model' => '\Miituu\Image',
            'multiple' => true
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
    );

    public function media() {
        return Media::where('id', $this->media_id);
    }

    public function answers() {
        return Answer::where('question_id', $this->id);
    }

    public function questionnaire() {
        return Questionnaire::where('id', $this->questionnaire_id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }

    public function validationMax($field_name) {
        
        $max = null;
        $rules = $this->validationRules($field_name);

        foreach($rules as $rule){

            if ( (strpos($rule, 'between:') !== false) ) {

                $range_str = substr( $rule, strpos($rule, ':')+1 );
                $range = explode(',', $range_str);
                $max = array_pop(  $range  );
            } elseif( (strpos($rule, 'max:') !== false) ) {

                $max = substr( $rule, strpos($rule, ':')+1 );
            }
        }

        return $max;
    }
}
