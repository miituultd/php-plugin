<?php

namespace Miituu;

class App extends Model {

	protected $path = 'apps';

	public $fields = array('id', 'company_id', 'platform', 'app_id', 'status', 'title', 'app_arn', 'created_at', 'updated_at', 'statistics', 'custom_data');

	public $mutable = array();

    public $json = array('custom_data');

	public $has_status   = true;

    public $status_titles   = array(
        self::STATUS_ACTIVE   => 'Active',
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_PENDING  => 'Pending',
        self::STATUS_DELETED  => 'Deleted'
    );

	public $relations = array(
		array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'devices',
            'model' => '\Miituu\Device',
            'multiple' => true
        )
	);

    // iOS / Android constants
    const PLATFORM_IOS     = 1;
    const PLATFORM_ANDROID = 2;

	public function devices() {
		return Device::where('app_id', $this->app_id);
	}

    public function getPlatform_title() {
        if ($this->platform == self::PLATFORM_IOS)
            return 'iOS';

        if ($this->platform == self::PLATFORM_ANDROID)
            return 'Android';

        return 'Unknown';
    }
}
