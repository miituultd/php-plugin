<?php

namespace Miituu;

class Questionnaire extends Model {

    protected $path = 'questionnaires';

    public $fields = array('id', 'company_id', 'name', 'description', 'featured', 'status', 'created_at', 'updated_at', 'user_id', 'disclaimer', 'thankyou', 'thankyou_url', 'code', 'search', 'statistics', 'custom_data', 'slideshow_mode', 'slideshow_background_color', 'slideshow_text_color');

    public $mutable = array('name', 'description', 'featured', 'status', 'disclaimer', 'thankyou', 'thankyou_url', 'image', 'custom_data');

    public $json = array('custom_data');

    public $has_status  = true;

    public $relations = array(
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        ),
        array(
            'key' => 'groups',
            'model' => '\Miituu\Group',
            'multiple' => true
        ),
        array(
            'key' => 'userfields',
            'model' => '\Miituu\Userfield',
            'multiple' => true
        ),
        array(
            'key' => 'questions',
            'model' => '\Miituu\Question',
            'multiple' => true
        ),
        array(
            'key' => 'images',
            'model' => '\Miituu\Image',
            'multiple' => true
        ),
        array(
            'key' => 'respondents',
            'model' => '\Miituu\Respondent',
            'multiple' => true
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
    );


    public function groups() {
        $tmp = Questionnaire::with('groups')->find($this->id);
        return $tmp->success ? $tmp->groups : null;
    }

    public function questions() {
        return Question::where('questionnaire_id', $this->id);
    }

    public function respondents() {
        return Respondent::where('questionnaire_id', $this->id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }


    public function _featured($featured = false) {
        return Questionnaire::where('featured', $featured ? 1 : 0);
    }

    public function feature() {
        if (!$this->exists())
            throw new \Exception('Featured state can only be changed on existing questionnaires.');

        return $this->save( array('featured' => 1) );
    }

    public function unfeature() {
        if (!$this->exists())
            throw new \Exception('Featured state can only be called on existing questionnaires.');

        return $this->save( array('featured' => 0) );
    }
}
