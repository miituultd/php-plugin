<?php

namespace Miituu;

class Campaign extends Model {

	protected $path = 'campaigns';

	public $fields = array('id', 'mailinglist_id', 'segment_id', 'questionnaire_id', 'name', 'title', 'body', 'send', 'complete', 'total', 'progress', 'created_at', 'updated_at');

	public $mutable = array('name', 'title', 'body', 'created_at', 'updated_at');

    public $inclusions = array('questionnaire', 'statistics');

	public $relations = array(
        array(
            'key' => 'mailinglist',
            'model' => '\Miituu\Mailinglist',
            'multiple' => false
        ),
        array(
            'key' => 'segment',
            'model' => '\Miituu\Segment',
            'multiple' => false
        ),
        array(
            'key' => 'addresses',
            'model' => '\Miituu\Address',
            'multiple' => true
        ),
        array(
            'key' => 'questionnaire',
            'model' => '\Miituu\Questionnaire',
            'multiple' => false
        )
    );

    public function mailinglist() {
    	return Mailinglist::where('id', $this->mailinglist_id);
    }

    public function questionnaire() {
    	return Questionnaire::where('id', $this->questionnaire_id);
    }

    public function segment() {
    	return Segment::where('id', $this->segment_id);
    }
}
