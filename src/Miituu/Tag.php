<?php

namespace Miituu;

class Tag extends Model {

	protected $path = 'tags';

	public $fields = array('id', 'name', 'status', 'created_at', 'updated_at');

	public $mutable = array('name', 'status', 'created_at', 'updated_at');

	public $has_status = true;

	public $relations = array(
		array(
            'key' => 'questions',
            'model' => '\Miituu\Question',
            'multiple' => true
        ),
        array(
            'key' => 'answers',
            'model' => '\Miituu\Answer',
            'multiple' => true
        ),
        array(
            'key' => 'media',
            'model' => '\Miituu\Media',
            'multiple' => true
        )
	);
}