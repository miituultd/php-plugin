<?php

namespace Miituu;

class Item extends Model {

    protected $path = 'items';

    public $fields = array('id', 'company_id', 'media_id', 'image_id', 'status', 'title', 'description', 'created_at', 'updated_at', 'likes', 'custom_data');

    public $mutable = array('status', 'title', 'description', 'custom_data');

    public $json = array('custom_data');

    public $has_status = true;

    public static $types = array('image', 'audio', 'video');

    public $relations = array(
        array(
            'key'       => 'playlists',
            'model'     => '\Miituu\Playlist',
            'multiple'  => true
        ),
        array(
            'key'       => 'media',
            'model'     => '\Miituu\Media',
            'multiple'  => false
        ),
        array(
            'key'       => 'company',
            'model'     => '\Miituu\Company',
            'multiple'  => false
        ),
        array(
            'key'       => 'image',
            'model'     => '\Miituu\Image',
            'multiple'  => false
        )
    );

    public function media() {
        return Media::where('id', $this->media_id);
    }

    public function image() {
        return Image::where('id', $this->image_id);
    }

    public function getType() {
        
        if ( $this->media_id && $this->media ) {
            return $this->media->type;
        } elseif ($this->image_id && $this->image ) {
            return 'image';
        } else {
            throw new \Exception('Unable to determine type');
        }
    }

    public function _like($id = null) {

        if( $this->exists() ) {
            $id = $this->id;
        } elseif( $id ) {
            $id = (int) $id;
        } else {
            throw new \Exception('Unable to detect id for this item');
        }

        return $this->call( '/like', array('id' => $id), 'POST' );
    }

    public function _unlike($id = null) {

        if( $this->exists() ) {
            $id = $this->id;
        } elseif( $id ) {
            $id = (int) $id;
        } else {
            throw new \Exception('Unable to detect id for this item');
        }

        return $this->call( '/unlike', array('id' => $id), 'POST' );
    }

    public function _type($type) {

        if(!in_array($type, Item::$types)) {
            throw new \Exception('The specified type is invalid');
        }

        return $this->where('type', $type);
    }
}
