<?php

namespace Miituu;

class Action extends Model {

    protected $path = 'actions';

    public $fields = array('id', 'company_id', 'user_id', 'actionable_id', 'actionable_type', 'type', 'created_at', 'updated_at', 'string');

    public $mutable = array();

    public $relations = array(
        array(
            'key'       => 'company',
            'model'     => '\Miituu\Company',
            'multiple'  => false
        ),
        array(
            'key'       => 'user',
            'model'     => '\Miituu\User',
            'multiple'  => false
        )
    );

    public function company() {
        return Company::where('id', $this->company_id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }
}
