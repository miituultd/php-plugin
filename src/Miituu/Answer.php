<?php

namespace Miituu;

class Answer extends Model {

    protected $path = 'answers';

    public $fields = array('id', 'question_id', 'respondent_id', 'media_id', 'text', 'type', 'multichoice', 'status', 'order', 'created_at', 'updated_at', 'rating', 'company_id', 'featured', 'description', 'search', 'custom_data', 'ticket_id');

    public $mutable = array('text', 'multichoice', 'status', 'rating', 'featured', 'description', 'custom_data', 'ticket_id');

    public $json = array('multichoice', 'custom_data');

    public $has_status = true;

    public static $types = array('text', 'image', 'audio', 'video', 'multichoice', 'document');

    public $inclusions = array('transcripts', 'questionnaire', 'tags', 'images', 'document', 'respondent', 'tags', 'question', 'media', 'media_item');

    public $relations = array(
        array(
            'key'       => 'media',
            'model'     => '\Miituu\Media',
            'multiple'  => false
        ),
        array(
            'key'       => 'images',
            'model'     => '\Miituu\Image',
            'multiple'  => true
        ),
        array(
            'key'       => 'question',
            'model'     => '\Miituu\Question',
            'multiple'  => false
        ),
        array(
            'key'       => 'respondent',
            'model'     => '\Miituu\Respondent',
            'multiple'  => false
        ),
        array(
            'key'       => 'transcripts',
            'model'     => '\Miituu\Transcript',
            'multiple'  => true
        ),
        array(
            'key'       => 'document',
            'model'     => '\Miituu\Document',
            'multiple'  => false
        ),
        array(
            'key' => 'ticket',
            'model' => '\Miituu\Ticket',
            'multiple' => false
        )
    );

    public function media() {
        return Media::where('id', $this->media_id);
    }

    public function question() {
        return Question::where('id', $this->question_id);
    }

    public function respondent() {
        return Respondent::with('userfieldanswers')->where('id', $this->respondent_id);
    }

    public function ticket() {
        return Ticket::where('id', $this->ticket_id);
    }

    public function _type($type) {

        if(!in_array($type, Answer::$types)) {
            throw new \Exception('The specified type is invalid');
        }

        return $this->where('type', $type);
    }

    public function _featured() {

        return $this->where('featured', 1);
    }

    public function _create($data) {
        // Strip back the data to what's fillable, but also include type, or a default

        $specialAllowed = array('type', 'question_id', 'respondent_id');

        $post = array();

        foreach ($data as $key => $value) {
            if (in_array($key, $this->mutable) || in_array($key, $specialAllowed)) {
                $post[$key] = $value;
            }
        }

        if (isset($data['document'])) {
            $this->file( 'document', $data['document'] );
        } else if (isset($data['image'])) {
            $this->file( 'image', $data['image'] );
        }

        return $this->call('', $post, 'POST');
    }

  public function _assignTicket( $ticket_id ) {
    return $this->call('/ticket', array(
      'id' => $this->id,
      'ticket_id' => $ticket_id,
    ), 'POST');
  }

    public function _findPromised($id, $params = array()) {
        return $this->call('/promised', array('id' => $id) + $params);
    }
}
