<?php

namespace Miituu;

class Userfield extends Model {

    // Constants used for the type field, and an array of all valid numbers
    const TYPE_OTHER   = 0;
    const TYPE_EMAIL   = 1;
    const TYPE_NAME    = 2;

    protected $path = null;

    public $fields = array('id', 'questionnaire_id', 'title', 'order', 'created_at', 'updated_at', 'required', 'status', 'type');

    public $mutable = array('title', 'order', 'required', 'status', 'type');

    public $relations = array(
        array(
            'key'       => 'questionnaire',
            'model'     => '\Miituu\Questionnaire',
            'multiple'  => false
        ),
        array(
            'key'       => 'userfieldanswer',
            'model'     => '\Miituu\Userfieldanswer',
            'multiple'  => true
        )
    );

    public function questionnaire() {
        return Questionnaire::where('id', $this->questionnaire_id);
    }

    /*
     *  Return a cleaned version of the userfield title
     */
    public function getName() {
        return preg_replace('/[^a-z]/', '', strtolower( $this->title ));
    }
}
