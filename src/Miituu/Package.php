<?php

namespace Miituu;

class Package extends Model {

    protected $path = 'billing';

    public $fields = array('id', 'name', 'storage', 'bandwidth', 'created_at', 'updated_at', 'users', 'code', 'emails');

    public $mutable = array('name', 'created_at', 'updated_at');

    public $relations = array(
        array(
            'key' => 'companies',
            'model' => '\Miituu\Company',
            'multiple' => true
        )
    );

    public function _currentPackage()
    {
        return $this->call('/package', array(), 'GET');
    }

    public function _getPackage($code)
    {
        return $this->call('/package/'.$code, array(), 'GET');
    }

    public function _allPackages()
    {
        return $this->call('/all_packages', array(), 'GET');
    }

    public function _check()
    {
        return $this->call('/check', array(), 'GET');
    }

    public function _upgrade( $package_ref )
    {
        if ( is_numeric($package_ref) ) {
            $params = array( 'package_id' => $package_ref );
        } else {
            $params = array( 'code' => $package_ref );
        }

        return $this->call('/change_package', $params, 'POST');
    }

    public function _subscribe( $recurly_token, $package_code )
    {
        $params = array(
            'recurly-token'     => $recurly_token,
            'code'              => $package_code
        );

        return $this->call('/subscribe', $params, 'POST');
    }

    public function _cancel( $reason = '', $password )
    {
        if(!$password){
            throw new \Exception('Please submit your password');
        }

        return $this->call('/cancel', array( 'reason' => $reason, 'password' => $password ) , 'POST');
    }
}