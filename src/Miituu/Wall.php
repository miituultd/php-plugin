<?php

namespace Miituu;

class Wall extends Model {

    protected $path = 'walls';

    public $fields = array( 'id', 'company_id', 'slug', 'status', 'custom_data' );

    public $mutable = array( 'slug', 'status', 'custom_data' );

    public $json = array('custom_data');

    public $endpoints = array( 'published' );

    public function getLink() {
        return 'http://' . $this->slug . $this->domain;
    }

    public function getAdmin_link() {
        return $this->link .'/admin';
    }

    public function getDomain() {
        return self::$wall_urls[self::$environment];
    }
}
