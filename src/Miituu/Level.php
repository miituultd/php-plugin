<?php

namespace Miituu;

class Level extends Model {

	protected $path = null;

	public $fields = array('id', 'name', 'slug', 'created_at', 'updated_at');

	public $mutable = array('name', 'slug', 'created_at', 'updated_at');

	public $relations = array(
		array(
            'key' => 'users',
            'model' => '\Miituu\User',
            'multiple' => true
        ),
        array(
            'key' => 'tokens',
            'model' => '\Miituu\Token',
            'multiple' => true
        )
	);
}