<?php

namespace Miituu;

class User extends Model {

    protected $path = 'users';

    public $fields = array('id', 'company_id', 'forename', 'surname', 'email', 'email_verified', 'email_verify_token', 'password', 'status', 'created_at', 'updated_at', 'level_id', 'permissions', 'custom_data');

    public $mutable = array('forename', 'surname', 'email', 'password', 'custom_data');

    public $json = array('custom_data');

    public $status_titles   = array(
        self::STATUS_ACTIVE   => 'Active',
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_DELETED  => 'Deleted'
    );

    public $level_titles   = array(
        self::LEVEL_PUBLIC  => 'Public',
        self::LEVEL_TRUSTED => 'Trusted',
        self::LEVEL_ADMIN   => 'Admin',
        self::LEVEL_OWNER   => 'Owner',
        self::LEVEL_MIITUU  => 'Miituu'
    );

    public $relations = array(
        array(
            'key'       => 'company',
            'model'     => '\Miituu\Company',
            'multiple'  => false
        ),
        array(
            'key'       => 'images',
            'model'     => '\Miituu\Image',
            'multiple'  => true
        ),
        array(
            'key'       => 'playlists',
            'model'     => '\Miituu\Playlist',
            'multiple'  => true
        ),
        array(
            'key'       => 'questions',
            'model'     => '\Miituu\Question',
            'multiple'  => true
        ),
        array(
            'key'       => 'questionnaires',
            'model'     => '\Miituu\Questionnaire',
            'multiple'  => true
        ),
        array(
            'key'       => 'tokens',
            'model'     => '\Miituu\Token',
            'multiple'  => true
        ),
        array(
            'key'       => 'level',
            'model'     => '\Miituu\Level',
            'multiple'  => false
        )
    );

    public function getFullname() {
        return $this->forename.' '.$this->surname;
    }

    public function getLevel_title() {
        return $this->level_titles[$this->level_id];
    }

    public function questionnaires() {
        return Questionnaire::where('user_id', $this->id);
    }

    public function _requestPasswordReset($email) {
        return $this->call('/request_password_reset', array('email' => $email), 'POST');
    }

    public function _resetPassword($reset, $password) {
        return $this->call('/reset_password', array(
            'reset'     => $reset,
            'password'  => $password
        ), 'POST');
    }

    public function _add( $params ) {
        return $this->call('/add', $params, 'POST');
    }

    public function _addPublic( $params ) {
        return $this->call('/add_public', $params, 'POST');
    }

    /*
        Get a list of permissions for a specified level
        Param may be 'level' or 'level_id'

        Value may be an integer or a string depending on whether the parameter is level(slug) or  level_id
        An integer value represents the level id
        A string value represents the level's slug
    */
    public function _allPermissions( $param = 'level', $value = 'public' ){

        if( $param == 'level' || $param == 'level_id' ) {
            return $this->call( '/all_permissions', array( $param => $value ), 'GET', true, true );
        } else {
            throw new \Exception('Invalid request, param must be level or level_id.');
        }
    }

}
