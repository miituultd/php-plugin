<?php

namespace Miituu;

class Upload extends Model {

    protected $path = 'uploads';

    public $fields = array('id', 'uid', 'status', 'media_id', 'image_id', 'name', 'chunks', 'chunks_done', 'created_at', 'updated_at');

    public $mutable = array();

    public $relations = array(
        array(
            'key'       => 'media',
            'model'     => '\Miituu\Media',
            'multiple'  => false
        )
    );

    /*
     *  Send a media file in one go
     */
    public function _media($config) {
        return $this->file('file', $config['file'])->call('/', array(
            'slug' => $config['slug'],
            'name' => $config['name'],
            'type' => $config['type']
        ), 'POST');
    }
}
