<?php

namespace Miituu;

class Address extends Model {

	protected $path = 'addresses';

	public $fields = array('id', 'mailinglist_id', 'status', 'name', 'address', 'created_at', 'updated_at');

	public $mutable = array('status', 'name', 'address', 'created_at', 'updated_at');

	public $has_status   = true;

	public $relations = array(
		array(
            'key'       => 'actions',
            'model'     => '\Miituu\Action',
            'multiple'  => true
        ),
        array(
            'key'       => 'campaigns',
            'model'     => '\Miituu\Campaign',
            'multiple'  => true
        ),
		array(
            'key'       => 'mailinglist',
            'model'     => '\Miituu\Mailinglist',
            'multiple'  => false
        )
	);

	public function mailinglist() {
		return Mailinglist::where('id', $this->mailinglist_id);
	}
}
