<?php

namespace Miituu;

class Image extends Model {

    protected $path = 'images';

    public $fields = array('id', 'company_id', 'user_id', 'questionnaire_id', 'question_id', 'answer_id', 'status', 'mime', 'width', 'height', 'filesize', 'url', 'orig_filename', 'filename', 'progress', 'title', 'created_at', 'updated_at', 'type', 'expires', 'url_original', 'slug', 'custom_data');

    public $mutable = array('title', 'status', 'custom_data');

    public $json = array('custom_data');

    public $has_status = false;

    public $relations = array(
        array(
            'key' => 'answer',
            'model' => '\Miituu\Answer',
            'multiple' => false
        ),
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        ),
        array(
            'key' => 'item',
            'model' => '\Miituu\Item',
            'multiple' => false
        ),
        array(
            'key' => 'question',
            'model' => '\Miituu\Question',
            'multiple' => false
        ),
        array(
            'key' => 'questionnaire',
            'model' => '\Miituu\Questionnaire',
            'multiple' => false
        ),
        array(
            'key' => 'user',
            'model' => '\Miituu\User',
            'multiple' => false
        )
    );


    public function answer() {
        return Answer::where('id', $this->answer_id);
    }

    public function question() {
        return Question::where('id', $this->question_id);
    }

    public function questionnaire() {
        return Questionnaire::where('id', $this->questionnaire_id);
    }

    public function user() {
        return User::where('id', $this->user_id);
    }

    /*
     *  Return a direct api or download URL for an image
     */
    public function permalink( $dynamic = false, $original = false ){

        // If a dynamic URL has been requested, we build it from the id etc
        if ( $dynamic ) {
            return Api::$base . $this->path . '/' . $this->id . ($original ? '/original' : '');
        }

        // But non-dynamic URLs go straight to the return S3 URL
        return $this->url;
    }

    public function _create($data) {
        // Strip back the data to what's fillable, but also include type, or a default
        $post = array_intersect($data, $this->mutable);
        $post['type'] = array_key_exists('type', $data) ? $data['type'] : 'default';

        return $this->file( 'image', $data['image'] )->call('', $post, 'POST');
    }
}
