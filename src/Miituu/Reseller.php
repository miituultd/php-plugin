<?php

namespace Miituu;

class Reseller extends Model {
	protected $path = 'resellers';

	public $fields = array( 'id', 'status', 'name', 'commission', 'statistics','created_at', 'updated_at' );

	public $mutable = array( 'name', 'commission', 'status' );

	public $json = array();

    public $has_status  = false;

    public $status_titles   = array(
        self::STATUS_ACTIVE   => 'Active',
        self::STATUS_INACTIVE  => 'Deleted'
    );

    public $relations = array(
        array(
            'key' => 'partnercodes',
            'model' => '\Miituu\Partnercode',
            'multiple' => true
        ),
        array(
            'key' => 'resells',
            'model' => '\Miituu\Resell',
            'multiple' => true
        )
    );
}