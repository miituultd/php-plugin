<?php

namespace Miituu;

class Key extends Model {

	protected $path = 'keys';

	public $fields = array('id', 'company_id', 'key', 'name', 'status', 'created_at', 'updated_at');

	public $mutable = array('key', 'name', 'status', 'created_at', 'updated_at');

    public $has_status = true;

    public $relations = array(
        array(
            'key' => 'company',
            'model' => '\Miituu\Company',
            'multiple' => false
        )
    );
}